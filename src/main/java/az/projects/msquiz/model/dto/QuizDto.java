package az.projects.msquiz.model.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
public class QuizDto {
    String categoryName;
    Integer numQuestions;
    String title;
}
